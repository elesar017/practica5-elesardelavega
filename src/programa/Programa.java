package programa;

import java.util.Scanner;

import clases.GestorMotos;

public class Programa {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		int opcion;
		/**
		 * Creamos la instancia de gestor
		 */
		GestorMotos gestor = new GestorMotos();
		
		/**
		 * Creo las 3 motos de la clase y listo
		 */
		gestor.altaMoto("KTM", "SM", 690, "2020-10-12", "SuperMotard", 123, 5.200);
		gestor.altaMoto("HUSQVARNA", "SQ", 550, "2018-05-15", "Enduro", 456, 3.540);
		gestor.altaMoto("RIEJU", "MARATHOLN", 300, "2019-11-25", "Cross", 789, 2.654);
	
		/**
		 * Creo los 3 due�os y los listo
		 */
		gestor.altaDueno("Elesar", "DeLaVega", 1111, "2000-11-25", "San Mateo", 21);
		gestor.altaDueno("Luky", "Tolos", 2222, "2000-07-17", "Zuera", 20);
		gestor.altaDueno("Izan", "Tabu", 3333, "2002-10-18", "Zaragoza", 18);
		
		/**
		 * Asignamos motos a los due�os
		 */
		gestor.asignarElemento(123, 2222);
		gestor.asignarElemento(456, 3333);
		gestor.asignarElemento(789, 1111);
		
		/**
		 * Creamos un do while para que se repita el menu
		 */
		do {
			
			/**
			 * Creamos el menu con las opciones que ir� dentro del switch 
			 */
			System.out.println("Elige una opcion con la que operar: ");
			System.out.println("------------------------------------");
			System.out.println("1 - Motos");
			System.out.println("2 - Due�os");
			System.out.println("3 - Finalizar");
			System.out.println("------------------------------------");
			opcion = input.nextInt();
			
			switch (opcion) {
			
			case 1: 
				/**
				 * En el caso 1 tenemos otro menu con las opciones de motos pero antes las listamos para saber que motos hay
				 */
				gestor.listarMotos();
				
				int opcion4;
				System.out.println("Elige una opcion dentro de motos:");
				System.out.println("------------------------------------");
				System.out.println("1 - Buscar moto por bastidor");
				System.out.println("2 - Eliminar moto por bastidor");
				System.out.println("3 - Listar motos mediante el nombre del due�o");
				System.out.println("4 - Ver el precio m�s mayor de una moto");
				System.out.println("5 - Ver el precio mas peque�o de una moto");
				System.out.println("------------------------------------");
				opcion4 = input.nextInt();
				
			/**
			 * Creamos otro switch con la opcion de moto que queramos usar y ya directamente entramos en el caso que marquemos
			 */
				switch (opcion4) {
				
				case 1:
					/**
					 * Buscar la moto por un atributo
					 */
					System.out.println("Buscamos la moto por su bastidor");
					int bastidorBuscar = input.nextInt();
					System.out.println(gestor.buscarMoto(bastidorBuscar));
					break;
				
				case 2:
					/**
					 * Eliminamos la moto por un atributo y las mostramos para comprobarlo
					 */
					System.out.println("Eliminamos la moto por el bastidor y mostramos las que quedan");
					int bastidorEliminar = input.nextInt();
					gestor.eliminarMoto(bastidorEliminar);
					gestor.listarMotos();
					break;
					
				case 3:
					/**
					 * Listamos la moto por el nombre del due�o
					 */
					input.nextLine();
					System.out.println("Escribe el nombre del due�o para listar");
					String nombreListar = input.nextLine();
					gestor.listarMotosDueno(nombreListar);
					break;
				case 4: 
					/**
					 * Conseguimos el mayor precio de una moto
					 */
					System.out.println("Vemos el mayor precio");
					gestor.mayorPrecioMoto();
					break;
				case 5:
					/**
					 * Vemos el menor precio de una moto
					 */
					System.out.println("Vemos el menor precio");
					gestor.menorPrecioMoto();
					break;
				default: System.out.println("Debes elegir una opcion entre 1 y 5");
				}
				
				break;
			
			case 2 : 
				/**
				 * En el caso 2 tenemos otro menu con las opciones de due�os pero antes los listamos para saber que due�os hay
				 */
				gestor.listarDueno();
				
				int opcion3;
				System.out.println("Elige una opcion dentro de due�os:");
				System.out.println("------------------------------------");
				System.out.println("1 - Buscar due�o por DNI");
				System.out.println("2 - Eliminar due�o por DNI");
				System.out.println("3 - Listar due�os mediante su DNI");
				System.out.println("4 - Ver el due�o m�s mayor con moto");
				System.out.println("5 - Ver que moto tiene la mayor cilindrada y su due�o");
				System.out.println("------------------------------------");
				opcion3 = input.nextInt();
				switch (opcion3) {
				case 1 :
					
				/**
				 * Buscar un elemento de Due�o
				 */
				System.out.println("Buscamos el due�o con DNI");
				int dniEscrito = input.nextInt();
				System.out.println(gestor.buscarDueno(dniEscrito));
				
							break;
				case 2 :
				/**
				 * Eliminar un elemento de Due�o y listar
				 */
					
				System.out.println("Eliminamos el due�o por DNI");
				
				int dniEliminar = input.nextInt();
				gestor.eliminarDueno(dniEliminar);
				gestor.listarDueno();
				
					break;
				
				case 3 :
					/**
					 * Listar el due�o por un atributo
					 */
					System.out.println("Listar due�o por su DNI");
					int due�oDNI = input.nextInt();
					System.out.println(gestor.listarDuenosAtributo(due�oDNI));
					break;
				case 4:
					/**
					 * Ver que la edad del due�o m�s mayor
					 */
					gestor.mayorEdadDueno();
					break;
				case 5:
					/**
					 * Vemos en nombre del due�o cuya moto sea de mahyor cilindrada (Marca de la moto tambien la vemos)
					 */
					gestor.MayorCilindradaDueno();
					break;
				
				default : System.out.println("Elegir opcion entre 1 y 4");
				}
				break;
			
			case 3 : 
				/**
				 * En el caso 3 salimos del programa
				 */
				System.out.println("Hasta luego, a dormir.");
				System.exit(0);
				break;
			default : System.out.println("Debes seleccionar una opcion entre 1 y 3");
		
			}
			/**
			 * Se repite el menu principal hasta que pulsemos la opcion 3 = finalizar
			 */
		} while(opcion != 3);
		input.close();
	}

}

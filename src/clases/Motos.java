package clases;

import java.time.LocalDate;

public class Motos {
	
	/**
	 * Creo los atributos de Motos
	 */
	private String marca;
	private String modelo;
	private int cilindrada;
	private LocalDate fechaCompra;
	private String tipoMoto;
	private int bastidor;
	private double precio;
	private Dueno due�oMoto;
	
	/**
	 * Creo el constructor de Motos con estos parametros 
	 * @param marca
	 * @param modelo
	 * @param cilindrada
	 * @param fechaCompra
	 * @param tipoMoto
	 * @param bastidor
	 * @param precio
	 */
	public Motos(String marca, String modelo, int cilindrada, String tipoMoto, int bastidor,
			double precio) {
		this.marca = marca;
		this.modelo = modelo;
		this.cilindrada = cilindrada;
		this.tipoMoto = tipoMoto;
		this.bastidor = bastidor;
		this.precio = precio;
	}
	
	/**
	 * Creo los setter y getter de la clase Motos con estos parametros que puse en el constructor
	 * @param marca
	 * @param modelo
	 * @param cilindrada
	 * @param fechaCompra
	 * @param tipoMoto
	 * @param bastidor
	 * @param precio
	 * @return due�oMoto
	 * @return marca
	 * @return modelo
	 * @return cilindrada
	 * @return fechaCompra
	 * @return tipoMoto
	 * @return bastidor
	 * @return precio
	 */
	
	public Dueno getDue�oMoto() {
		return due�oMoto;
	}

	public void setdue�oMoto(Dueno due�oMoto) {
		this.due�oMoto = due�oMoto;
	}
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}

	public LocalDate getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getTipoMoto() {
		return tipoMoto;
	}

	public void setTipoMoto(String tipoMoto) {
		this.tipoMoto = tipoMoto;
	}

	public int getBastidor() {
		return bastidor;
	}

	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	/**
	 * Creo el ToString con casi todos los atributos de Motos
	 */
	@Override
	public String toString() {
		return "Motos [marca=" + marca + ", modelo=" + modelo + ", cilindrada=" + cilindrada + ", fechaCompra="
				+ fechaCompra + ", tipoMoto=" + tipoMoto + ", bastidor=" + bastidor + ", precio=" + precio + "]";
	}
	
}

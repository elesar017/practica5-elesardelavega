package clases;

import java.time.LocalDate;

public class Dueno {
	
		/**
		 * Creo los atributos de Due�o
		 */
		private String nombre;
		private String apellido;
		private int dni;
		private LocalDate fechaNacimiento;
		private String localidad;
		private int edad;
		private Motos motoAsignada;
		
		
		/**
		 * Creo el constructor de Due�o con sus atributos
		 * @param nombre
		 * @param apellido
		 * @param dni
		 * @param localidad
		 * @param edad
		 */
		public Dueno(String nombre, String apellido, int dni, String localidad, int edad) {
			this.nombre = nombre;
			this.apellido = apellido;
			this.dni = dni;
			this.localidad = localidad;
			this.edad = edad;
		}
		
		/**
		 * Creo los setter y getter con los atributos anteriores
		 * @param nombre
		 * @param motoAsignada
		 * @param apellido
		 * @param dni
		 * @param localidad
		 * @param edad
		 * @return nombre
		 * @return motoAsignada
		 * @return apellido
		 * @return dni
		 * @return localidad
		 * @return edad
		 */
		public Motos getmotoAsignada() {
			return motoAsignada;
		}

		public void setmotoAsignada(Motos motoAsignada) {
			this.motoAsignada = motoAsignada;
		}
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getApellido() {
			return apellido;
		}

		public void setApellido(String apellido) {
			this.apellido = apellido;
		}

		public int getDni() {
			return dni;
		}

		public void setDni(int dni) {
			this.dni = dni;
		}

		public LocalDate getFechaNacimiento() {
			return fechaNacimiento;
		}

		public void setFechaNacimiento(LocalDate fechaNacimiento) {
			this.fechaNacimiento = fechaNacimiento;
		}

		public String getLocalidad() {
			return localidad;
		}

		public void setLocalidad(String localidad) {
			this.localidad = localidad;
		}

		public int getEdad() {
			return edad;
		}

		public void setEdad(int edad) {
			this.edad = edad;
		}
		
		/**
		 * Creo el ToString de �sta clase
		 */
		@Override
		public String toString() {
			return "Due�o [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", fechaNacimiento="
					+ fechaNacimiento + ", localidad=" + localidad + ", edad=" + edad + "]";
		}

	
}

package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author ELE
 * Clase en al que se guardan los metodos que haremos
 */
public class GestorMotos {
	
	private ArrayList<Motos> listaMotos;
	private ArrayList<Dueno> listaDueno;
	
	/**
	 * constructor de la gestion de las motos
	 * @param listaMotos
	 * @param listaDueno
	 */
	public GestorMotos() {
		this.listaMotos = new ArrayList<Motos>();
		this.listaDueno = new ArrayList<Dueno>();
	}

	/**
	 * metodo que comprobara si existe una moto
	 * @param bastidor; el unico codigo que debe ser diferente en cada moto
	 * @return un boolean que nos dir� si existe(true)  o no existe (false)
	 */
	public boolean existeMoto(int bastidor) {
		for (Motos motos : listaMotos) {
			if (motos != null && motos.getBastidor()==bastidor) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * metodo que nos listar� las motos que tengamos (con for normal)
	 * ni recibe ni devuelve ningun parametro
	 */
	public void listarMotos () {
		for (int i = 0; i < listaMotos.size(); i++) {
			if (listaMotos != null ) {
				System.out.println(listaMotos.get(i));
			}
		}
	}
	
	
	/**
	 * Metodo que da de alta las motos
	 * 
	 * @param a el valor de los atributos de cada moto 
	 */
	public void altaMoto(String marca, String modelo, int cilindrada, String fechaCompra, String tipoMoto, int bastidor,double precio) {
		Motos nuevaMoto = new Motos(marca, modelo, cilindrada, tipoMoto, bastidor, precio);
		nuevaMoto.setFechaCompra(LocalDate.parse(fechaCompra));
			listaMotos.add(nuevaMoto);
	}
	
	/**
	 * metodo para buscar una moto con foreach
	 * @param bastidor; unico parametros unico de cada moto
	 * @return la moto que has buscado si la ha encontrado o sino no devuelve nada
	 */
	public Motos buscarMoto(int bastidor) {
		for (Motos motos : listaMotos) {
			if (motos != null && motos.getBastidor()==bastidor) {
				return motos;
			}
		}
		return null;
	}
	
	/**
	 * metodo con iterator que elimina una moto
	 * @param bastidor
	 */
	public void eliminarMoto(int bastidor) {
		Iterator<Motos> iteratorMotos = listaMotos.iterator();
		
		while (iteratorMotos.hasNext()) {
			Motos motos = iteratorMotos.next();
			if (motos.getBastidor()==bastidor) {
				iteratorMotos.remove();
			}
		}
	}
	
	/**
	 * metodo que comprobara si existe un due�o
	 * @param int; el unico codigo que debe ser diferente en cada due�o
	 * @return un boolean que nos dir� si existe(true) o no existe (false)
	 */
	public boolean existeDueno(int dni) {
		for (Dueno due�o : listaDueno) {
			if (due�o != null && due�o.getDni()==dni) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * metodo que nos listar� los due�os de motos que tengamos (con for normal)
	 * ni recibe ni devuelve ningun parametro
	 */
	public void listarDueno () {
		for (int i = 0; i < listaDueno.size(); i++) {
			if (listaDueno != null ) {
				System.out.println(listaDueno.get(i));
			}
		}
	}
	
	/**
	 * Metodo que da de alta las motos
	 * 
	 * @param a el valor de los atributos de cada moto 
	 */
	public void altaDueno(String nombre, String apellido, int dni, String fechaNacimiento, String localidad, int edad) {
		Dueno nuevoDueno = new Dueno(nombre,apellido,dni,localidad,edad);
		nuevoDueno.setFechaNacimiento(LocalDate.parse(fechaNacimiento));
		listaDueno.add(nuevoDueno);
	}
	
	/**
	 * metodo para buscar un due�o con foreach
	 * @param dni; unico parametros unico de cada due�o
	 * @return el due�o que has buscado si lo ha encontrado o sino no devuelve nada
	 */
	public Dueno buscarDueno(int dni) {
		for (Dueno due�o : listaDueno) {
			if (due�o != null && due�o.getDni()==dni) {
				return due�o;
			}
		}
		return null;
	}
	
	/**
	 * metodo con iterator que elimina un due�o
	 * @param dni
	 */
	public void eliminarDueno(int dni) {
		Iterator<Dueno> iteratorDue�o = listaDueno.iterator();
		
		while (iteratorDue�o.hasNext()) {
			Dueno duenos = iteratorDue�o.next();
			if (duenos.getDni()==dni) {
				iteratorDue�o.remove();
			}
		}
	}
	
	/**
	 * metodo que lista los due�os por el dni
	 * @param dni
	 * @return
	 */
	public Dueno listarDuenosAtributo(int dni) {
		for (Dueno duenos : listaDueno) {
			if (duenos != null && duenos.getDni()==dni) {
				return duenos;
			}
		}
		return null;
	}
	/**
	 * metodo para listar moto con el nombre del due�o
	 * @param nombre
	 */
	public void listarMotosDueno (String nombre) {
		for (Motos moto : listaMotos) {
			if (moto.getDue�oMoto() != null && moto.getDue�oMoto().getNombre().equals(nombre)) {
				System.out.println(moto);
			}
		}
	}
	
	/**
	 * metodo para asignar al due�o una moto nueva
	 * @param bastidor y nombre
	 * @param nombre
	 */
	public void asignarElemento (int bastidor, int dni) {
		if (buscarDueno(dni) != null && buscarMoto(bastidor) != null) {
			Dueno duenos = buscarDueno(dni);
			Motos moto = buscarMoto(bastidor);
			duenos.setmotoAsignada(moto);
			moto.setdue�oMoto(duenos);
		}
	}
	
	/**
	 * Hago un m�todo donde scar� el mayor precio de Todas las motos
	 */
	public void mayorPrecioMoto () {
		double mayorPrecio = 0.0;
		for (Motos moto : listaMotos) {
			if (moto != null) {
				mayorPrecio = moto.getPrecio();
				if (moto.getPrecio() > mayorPrecio) {
					mayorPrecio = moto.getPrecio();
				}
			}
		}
		System.out.println("El mayor precio es " + mayorPrecio);
	}
	
	/**
	 * Creo un metodo como el anterior pero donde nos ense�ar� el menor precio
	 */
	public void menorPrecioMoto () {
		double mayorPrecio = 0.0;
		for (Motos moto : listaMotos) {
			if (moto != null) {
				mayorPrecio = moto.getPrecio();
				if (moto.getPrecio() < mayorPrecio) {
					mayorPrecio = moto.getPrecio();
				}
			}
		}
		System.out.println("El mayor precio es " + mayorPrecio);
	}
	
	/**
	 * �ste metodo nos indica que due�o es m�s mayor
	 */
	public void mayorEdadDueno() {
		int edad = 0;
		for (Dueno duenos : listaDueno) {
			if (duenos != null && duenos.getmotoAsignada() != null) {
				if (duenos.getEdad() > edad) {
					edad = duenos.getEdad();
				}
			}
		}
		System.out.println("El due�o m�s mayor tiene " + edad);
	}
	
	/**
	 * Aqu� sacamos el nombre de la moto y del due�o cuya moto sea de mayor cilindrada
	 */
	public void MayorCilindradaDueno() {
		String nombreMoto = null;
		String duenoNombre = null;
		int mayorCilindrada = 0;
			for (Motos moto : listaMotos) {
				if (moto != null) {
					if (moto.getCilindrada() > mayorCilindrada) {
						mayorCilindrada = moto.getCilindrada();
						nombreMoto = moto.getMarca();
						duenoNombre = moto.getDue�oMoto().getNombre();
					}
				}
			}
		System.out.println("La mayor cilindrada es de la moto " + nombreMoto + " y su due�o es " + duenoNombre);
		}
	}

